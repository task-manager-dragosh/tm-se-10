package ru.dragosh.tm.api;

import ru.dragosh.tm.entity.Task;

import java.util.List;

public interface TaskRepository extends Repository<Task> {
    List<Task> findAll(String userId, String projectId);
    Task find(String userId, String projectId, String nameTask);
    void removeAll(String userId, String projectId);
    List<Task> findByStringPart(String userId, String projectId, String str);
}