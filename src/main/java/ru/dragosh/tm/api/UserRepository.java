package ru.dragosh.tm.api;

import ru.dragosh.tm.entity.User;

import java.util.List;

public interface UserRepository {
    User find(String login, String password);
    User findByLogin(String login);
    void persist(User user);
    void merge(User user);
    void remove(String userId);

    List<User> getEntitiesList();
    void loadEntities(List<User> entities);
}
