package ru.dragosh.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.dragosh.tm.command.AbstractCommand;
import ru.dragosh.tm.enumeration.RoleType;

import java.util.HashSet;
import java.util.Set;

public final class RemoveAllProjectsCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "remove all projects";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "(удаляет все проекты и связанные с ними задачи из баз данных Projects и Tasks)";
    }

    @Override
    public void execute() {
        serviceLocator.getProjectService().removeAll(serviceLocator.getCurrentUser().getId());
    }

    @NotNull
    @Override
    public Set<RoleType> getRoles() {
        return new HashSet<RoleType>() {{
            add(RoleType.ADMIN);
            add(RoleType.USER);
        }};
    }
}