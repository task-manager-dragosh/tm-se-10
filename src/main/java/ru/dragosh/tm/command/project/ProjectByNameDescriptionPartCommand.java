package ru.dragosh.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.dragosh.tm.command.AbstractCommand;
import ru.dragosh.tm.enumeration.RoleType;
import ru.dragosh.tm.exception.CommandExecutionException;
import ru.dragosh.tm.util.ConsoleUtil;
import ru.dragosh.tm.util.MessageType;

import java.util.HashSet;
import java.util.Set;

public final class ProjectByNameDescriptionPartCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "find project by name part";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "(поиск проектов по части названия)";
    }

    @Override
    public void execute() throws CommandExecutionException {
        String projectNameOrDescriptionPart = readWord("Введите название или описание проекта: ");
        if (projectNameOrDescriptionPart == null || projectNameOrDescriptionPart.isEmpty()) {
            ConsoleUtil.log(MessageType.WRONG_DATA_FORMAT);
            return;
        }
        serviceLocator.getProjectService().findByStringPart(serviceLocator.getCurrentUser().getId(), projectNameOrDescriptionPart);
    }

    @NotNull
    @Override
    public Set<RoleType> getRoles() {
        return new HashSet<RoleType>() {{
            add(RoleType.USER);
            add(RoleType.ADMIN);
        }};
    }
}
