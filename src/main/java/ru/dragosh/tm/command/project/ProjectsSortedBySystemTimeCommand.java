package ru.dragosh.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.dragosh.tm.command.AbstractCommand;
import ru.dragosh.tm.enumeration.RoleType;
import ru.dragosh.tm.exception.CommandExecutionException;
import ru.dragosh.tm.util.ConsoleUtil;

import java.util.HashSet;
import java.util.Set;

public final class ProjectsSortedBySystemTimeCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "sort projects by date finish";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "(вывод проектов отсортированных по дате окончания)";
    }

    @Override
    public void execute() throws CommandExecutionException {
        serviceLocator.getProjectService().getSortedBySystemTime(serviceLocator.getCurrentUser().getId()).forEach(ConsoleUtil::projectOutput);
    }

    @NotNull
    @Override
    public Set<RoleType> getRoles() {
        return new HashSet<RoleType>() {{
            add(RoleType.USER);
            add(RoleType.ADMIN);
        }};
    }
}
