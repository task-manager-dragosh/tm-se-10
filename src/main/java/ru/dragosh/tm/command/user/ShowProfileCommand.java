package ru.dragosh.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.dragosh.tm.command.AbstractCommand;
import ru.dragosh.tm.enumeration.RoleType;
import ru.dragosh.tm.util.ConsoleUtil;

import java.util.HashSet;
import java.util.Set;

public final class ShowProfileCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "show profile";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "(вывод информации о профиле пользователя)";
    }

    @Override
    public void execute() {
        ConsoleUtil.userOutput(serviceLocator.getCurrentUser());
    }

    @NotNull
    @Override
    public Set<RoleType> getRoles() {
        return new HashSet<RoleType>() {{
            add(RoleType.ADMIN);
            add(RoleType.USER);
        }};
    }
}
