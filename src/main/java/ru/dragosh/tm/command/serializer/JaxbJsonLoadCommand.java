package ru.dragosh.tm.command.serializer;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.dragosh.tm.api.Serializer;
import ru.dragosh.tm.command.AbstractCommand;
import ru.dragosh.tm.entity.Domain;
import ru.dragosh.tm.entity.User;
import ru.dragosh.tm.enumeration.RoleType;
import ru.dragosh.tm.exception.CommandExecutionException;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public final class JaxbJsonLoadCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "jaxb load bases in json";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "(загрузка баз данных в формате json с помощью jaxb)";
    }

    @Override
    public void execute() throws CommandExecutionException {
        @NotNull final Serializer jaxbJsonService = serviceLocator.getJaxbJsonService();
        @Nullable Domain domain = null;
        try {
            domain = jaxbJsonService.Load();
        } catch (IOException | JAXBException e) {
            System.out.println("MESSAGE -> " + this.getClass().getName() + ": Ошибка при загрузке данных!");
        }
        if (domain == null)
            return;
        if (domain.getProjectList() == null)
            serviceLocator.getProjectService().loadEntities(Collections.emptyList());
        else
            serviceLocator.getProjectService().loadEntities(domain.getProjectList());
        if (domain.getTaskList() == null)
            serviceLocator.getTaskService().loadEntities(Collections.emptyList());
        else
            serviceLocator.getTaskService().loadEntities(domain.getTaskList());
        if (domain.getUserList() == null)
            serviceLocator.getUserService().loadEntities(new ArrayList<User>() {{
                add(new User("root", "root", RoleType.ADMIN));
                add(new User("user", "user", RoleType.USER));
            }});
        else
            serviceLocator.getUserService().loadEntities(domain.getUserList());

        serviceLocator.setCurrentUser(new User("", "", RoleType.INCOGNITO));
    }

    @NotNull
    @Override
    public Set<RoleType> getRoles() {
        return new HashSet<RoleType>() {{
            add(RoleType.ADMIN);
        }};
    }
}
