package ru.dragosh.tm.command.serializer;

import org.jetbrains.annotations.NotNull;
import ru.dragosh.tm.api.Serializer;
import ru.dragosh.tm.command.AbstractCommand;
import ru.dragosh.tm.entity.Domain;
import ru.dragosh.tm.enumeration.RoleType;
import ru.dragosh.tm.exception.CommandExecutionException;

import java.util.HashSet;
import java.util.Set;

public final class FasterJsonSaveCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "faster save bases in json";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "(сохранение баз данных в формате json с помощью faster)";
    }

    @Override
    public void execute() throws CommandExecutionException {
        @NotNull final Serializer fasterJsonService = serviceLocator.getFasterJsonService();
        @NotNull final Domain domain = new Domain();
        domain.setProjectList(serviceLocator.getProjectService().getEntitiesList());
        domain.setTaskList(serviceLocator.getTaskService().getEntitiesList());
        domain.setUserList(serviceLocator.getUserService().getEntitiesList());
        try {
            fasterJsonService.save(domain);
        } catch (Exception e) {
            System.out.println("MESSAGE -> " + this.getClass().getName() + ": Ошибка при сохранении данных");
        }
    }

    @NotNull
    @Override
    public Set<RoleType> getRoles() {
        return new HashSet<RoleType>() {{
            add(RoleType.ADMIN);
        }};
    }
}
