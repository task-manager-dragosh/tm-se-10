package ru.dragosh.tm.command.serializer;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.dragosh.tm.api.Serializer;
import ru.dragosh.tm.command.AbstractCommand;
import ru.dragosh.tm.entity.Domain;
import ru.dragosh.tm.enumeration.RoleType;
import ru.dragosh.tm.exception.CommandExecutionException;

import java.util.HashSet;
import java.util.Set;

public final class BinDataSaveCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "save bases in bytes";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "(сохранение баз данных в последовательность байтов)";
    }

    @Override
    public void execute() throws CommandExecutionException {
        @NotNull final Serializer dataBinService = serviceLocator.getDataBinService();
        @Nullable final Domain domain = new Domain();
        domain.setProjectList(serviceLocator.getProjectService().getEntitiesList());
        domain.setTaskList(serviceLocator.getTaskService().getEntitiesList());
        domain.setUserList(serviceLocator.getUserService().getEntitiesList());
        try {
            dataBinService.save(domain);
        } catch (Exception e) {
            System.out.println("MESSAGE -> " + this.getClass().getName() + ": Ошибка при сохранении данных");
        }
    }

    @NotNull
    @Override
    public Set<RoleType> getRoles() {
        return new HashSet<RoleType>() {{
            add(RoleType.ADMIN);
        }};
    }
}
