package ru.dragosh.tm.command.serializer;

import org.jetbrains.annotations.NotNull;
import ru.dragosh.tm.api.Serializer;
import ru.dragosh.tm.command.AbstractCommand;
import ru.dragosh.tm.entity.Domain;
import ru.dragosh.tm.enumeration.RoleType;
import ru.dragosh.tm.exception.CommandExecutionException;

import java.util.HashSet;
import java.util.Set;

public final class FasterXmlSaveCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "faster save bases in xml";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "(сохранение баз данных в формате xml с помошью faster)";
    }

    @Override
    public void execute() throws CommandExecutionException {
        @NotNull final Serializer fasterXmlService = serviceLocator.getFasterXmlService();
        @NotNull final Domain domain = new Domain();
        domain.setProjectList(serviceLocator.getProjectService().getEntitiesList());
        domain.setTaskList(serviceLocator.getTaskService().getEntitiesList());
        domain.setUserList(serviceLocator.getUserService().getEntitiesList());
        try {
            fasterXmlService.save(domain);
        } catch (Exception e) {
            System.out.println("MESSAGE -> " + this.getClass().getName() + ": Ошибка при сохранении данных");
        }
    }

    @NotNull
    @Override
    public Set<RoleType> getRoles() {
        return new HashSet<RoleType>() {{
            add(RoleType.ADMIN);
        }};
    }
}
