package ru.dragosh.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.dragosh.tm.api.TaskRepository;
import ru.dragosh.tm.api.TaskService;
import ru.dragosh.tm.entity.Task;

import java.util.Collections;
import java.util.List;

public final class TaskServiceImplement extends AbstractService<Task, TaskRepository> implements TaskService {
    @NotNull
    private final TaskRepository taskRepository;

    public TaskServiceImplement(@NotNull final TaskRepository taskRepository) {
        super(taskRepository);
        this.taskRepository = taskRepository;
    }

    @NotNull
    @Override
    public List<Task> findAll(@NotNull final String userId, @NotNull final String projectId) {
        if (userId == null || userId.isEmpty())
            return Collections.emptyList();
        if (projectId == null || projectId.isEmpty())
            return Collections.emptyList();
        return taskRepository.findAll(userId, projectId);
    }

    @Nullable
    @Override
    public Task find(@NotNull final String userId, @NotNull final String projectId, @NotNull final String nameTask) {
        if (userId == null || userId.isEmpty())
            return null;
        if (projectId == null || projectId.isEmpty())
            return null;
        if (nameTask == null || nameTask.isEmpty())
            return null;
        return taskRepository.find(userId, projectId, nameTask);
    }

    @Override
    public void removeAll(@NotNull final String userId, @NotNull final String projectId) {
        if (userId == null || userId.isEmpty())
            return;
        if (projectId == null || projectId.isEmpty())
            return;
        taskRepository.removeAll(userId, projectId);
    }

    @NotNull
    @Override
    public List<Task> findByStringPart(@NotNull final String userId, @NotNull final String projectId, @NotNull final String str) {
        if (userId == null || userId.isEmpty())
            return Collections.emptyList();
        if (projectId == null || projectId.isEmpty())
            return Collections.emptyList();
        if (str == null || str.isEmpty())
            return Collections.emptyList();
        return taskRepository.findByStringPart(userId, projectId, str);
    }

    @NotNull
    @Override
    public List<Task> getSortedBySystemTime(@NotNull final String userId) {
        if (userId == null || userId.isEmpty())
            return Collections.emptyList();
        return taskRepository.getSortedBySystemTime(userId);
    }

    @NotNull
    @Override
    public List<Task> getSortedByDateStart(@NotNull final String userId) {
        if (userId == null || userId.isEmpty())
            return Collections.emptyList();
        return taskRepository.getSortedByDateStart(userId);
    }

    @NotNull
    @Override
    public List<Task> getSortedByDateFinish(@NotNull final String userId) {
        if (userId == null || userId.isEmpty())
            return Collections.emptyList();
        return taskRepository.getSortedByDateFinish(userId);
    }

    @NotNull
    @Override
    public List<Task> getSortedByStatus(@NotNull final String userId) {
        if (userId == null || userId.isEmpty())
            return Collections.emptyList();
        return taskRepository.getSortedByStatus(userId);
    }
}
