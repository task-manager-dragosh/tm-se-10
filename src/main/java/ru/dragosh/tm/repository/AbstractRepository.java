package ru.dragosh.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.dragosh.tm.api.Repository;
import ru.dragosh.tm.entity.Entity;

import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public abstract class AbstractRepository<E extends Entity> implements Repository<E> {
    @NotNull protected final Map<String, E> base = new LinkedHashMap<>();

    @Override
    public void persist(@NotNull final E entity) {
        base.put(entity.getId(), entity);
    }

    @Override
    public void merge(@NotNull final E entity) {
        base.put(entity.getId(), entity);
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final String entityId) {
        base.values().stream()
                .filter(entity -> entity.getUserId().equals(userId))
                .findFirst().ifPresent(entity -> base.remove(userId));
    }

    @NotNull
    @Override
    public List<E> getSortedBySystemTime(@NotNull final String userId) {
        Comparator<E> comparator = Comparator.comparingLong(Entity::getSystemTime);
        return base.values().stream()
                .filter(entity -> entity.getUserId().equals(userId))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<E> getSortedByDateStart(@NotNull final String userId) {
        Comparator<E> comparator = Comparator.comparing(Entity::getDateStart);
        return base.values().stream()
                .filter(entity -> entity.getUserId().equals(userId))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<E> getSortedByDateFinish(@NotNull final String userId) {
        Comparator<E> comparator = Comparator.comparing(Entity::getDateFinish);
        return base.values().stream()
                .filter(entity -> entity.getUserId().equals(userId))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<E> getSortedByStatus(@NotNull final String userId) {
        Comparator<E> comparator = Comparator.comparing(entity -> entity.getStatus().getStatusName());
        return base.values().stream()
                .filter(entity -> entity.getUserId().equals(userId))
                .sorted(comparator)
                .collect(Collectors.toList());
    }
}
